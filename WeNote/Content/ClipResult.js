$(document).ready(function(){

	window.addEventListener('message', function(event) {
		var request = event.data;
		if(request.name=="ClipResult") {
			var iframe_document = $(document);
			iframe_document.find("#share_info").html(request.share_info);
			iframe_document.find("#share_title").html("<a target='_blank' href='" + request.url + "'>" + request.title + "</a>");

		}
	});
	$("#btn_close_ss").click(function(){
		window.parent.postMessage({
			name:"Close",
			id:"Clip_Result"
		},'*');
	});
});