function Clipper() {
    function k(b) {
        for (var c = b.querySelectorAll("a.clearly_highlight_delete_element"), a = 0; a < c.length; a++)
            c.item(a).parentNode.removeChild(c.item(a));
        b = b.querySelectorAll("em.clearly_highlight_element");
        for (a = 0; a < b.length; a++)
            b.item(a).outerHTML = "<highlight>" + b.item(a).innerHTML + "</highlight>"
    }
    function countSerializableFrames() {
        for (var a = document.querySelectorAll("iframe"), c = 0, b = 0; b < a.length; b++) a[b].dataset && a[b].dataset.en_id && c++;
        return c
    }
    function completedFrameCount() {
        var a = 0,
            c;
        for (c in frameData) a++;
        return a
    }
    function serializeFrames(callBack) {
        function c() {
            completed || (console.log("Some frames seem stuck, continuing with what we've got."),
                completed = !0,
                callBack(frameData))
        }
        0 == countSerializableFrames() ?
            callBack(null) :
            (addedEventListeners || (
                window.addEventListener("message",
                    function (b) {
                        b && b.data && b.data.name && "EN_serialized" == b.data.name ?
                            (frameData[b.data.id] = b.data.data, clearTimeout(timeout),
                            completed || (completedFrameCount() == countSerializableFrames() ?
                                (completed = !0, callBack(frameData)) :
                                timeout = setTimeout(c, 2E3))) :
                        b && b.data && b.data.name && "main_getTextResource" == b.data.name && Browser.sendToExtension({
                            name: "main_getTextResource",
                            href: b.data.href
                        })
                    },
                    !1), addedEventListeners = !0), frameData = {},
                completed = !1, timeout = setTimeout(c, 2E3),
                Browser.sendToExtension({
                    name: "bounce",
                    message: {
                        name: "startSerialize"
                    }
                })
            )
    }

    /**
     *
     * @param selectContainer
     * @param selectionRange
     * @param isDDsa
     * @param callBack
     * @param d
     * @param errorCallBack
     */
    function startClip(selectContainer, selectionRange, isDDsa, callBack, d, errorCallBack) {
        function f(a, b) {
            b ? errorCallBack(b) : CallBack(a)
        }
        SelectContainer = selectContainer;
        SelectionRange = selectionRange;
        IsIncludeCss = isDDsa;
        CallBack = callBack;
        if (d)
            p.serialize(SelectContainer, SelectionRange, IsIncludeCss, f, null, d);
        else {
            selectContainer = function (a) {
                p.serialize(SelectContainer, SelectionRange, IsIncludeCss, f, a)
            };
            try {
                serializeFrames(selectContainer)
            } catch (h) {
                errorCallBack(h)
            }
        }
    }
    var p = new HtmlSerializer,
        SelectContainer, SelectionRange, IsIncludeCss, CallBack;
    this.clipFullPage = function (isIncludeCss, callBack, errorCallBack) {
        k(document.body);
        startClip(document.body, null, isIncludeCss, callBack, null, errorCallBack)
    };
    /**剪切节点
     *
     * @param isIncludeCss bool?
     * @param selectionText 选中的文字
     * @param callBack 回调函数
     * @param errorCallBack 发生错误时的回调函数
     */
    this.clipSelection = function (isIncludeCss, selectionText, callBack, errorCallBack) {
        if (selectionText && document.querySelector("embed[type='application/pdf']")) a(selectionText);
        else {
            var selection, selectionRange;
            try {
                //window.getSelection()
                //if (selection = contentPreview.ensureSelectionIsShown())
                if (selection = window.getSelection())
                    if (selectionRange = selection.getRangeAt(0))
                        if (selectionRange.commonAncestorContainer.nodeType == Node.TEXT_NODE) {
                            var selectText = selectionRange.commonAncestorContainer.textContent.substring(selectionRange.startOffset, selectionRange.endOffset),
                                selectText = selectText.replace(/&/g, "&amp;"),
                                selectText = selectText.replace(/</g, "&lt;"),
                                selectText = selectText.replace(/>/g, "&gt;");
                            callBack(selectText);
                        } else
                            k(selectionRange.commonAncestorContainer),
                                startClip(selectionRange.commonAncestorContainer, selectionRange, isIncludeCss, callBack, null, errorCallBack)
            } catch (l) {
                errorCallBack("")
            }
        }
    };
    this.clipUrl = function (url,title,callBack,errorCallBack) {
        try{
            function re(title,url){
                url = "<a target='_blank' href='" + url + "'>" + title + "</a><br/> 地址:" +url;
                callBack(url);
            }

            if(url===window.location.href){
                re(window.document.title,url);
                return;
            }
            $(document).find("a").each(function (i, v) {
                if (v.href = url) {
                    title = v.text;
                    re(title,url);
                    return;
                }
            });

            for (item in window.frames) {
                $(item.contentWindow.document).find("a").each(function (i, v) {
                    if (v.href = url) {
                        title = v.text;
                        re(title,url);
                        return;
                    }
                });
            }
        }catch (e){
            errorCallBack(e);
        }

    };
    this.clipImage=function(url,callBack,errorCallBack){
        try {
            $(document).find("img").each(function (i, v) {
                if (v.src = url) {
                    url = p.convertImgSrcToBase64(v, url);
                    callBack(url);
                    return false;
                }
            })
            for (item in window.frames) {
                $(item.contentWindow.document).find("img").each(function (i, v) {
                    if (v.src = url) {
                        url = p.convertImgSrcToBase64(v, url);
                        callBack(url);
                        return false;
                    }
                });
            }
        }catch (e){
            errorCallBack(e);
        }
    };
    Object.preventExtensions(this)
}
Object.preventExtensions(Clipper);