﻿var clipper;
var selectionText = "";

window.addEventListener('message', function(event) {
    var request = event.data;
    if(request.name==="Close") {
        var frame = $(this.document).find("#"+request.id);
        frame.animate({
                opacity:0,
                height:"0px"
            },200,function(){
            frame.remove();
        });
    }
});

/*
后台消息监听
 */
chrome.extension.onMessage.addListener(function (request, sender, sendResponse) {
    var title = document.title;
    if(!clipper){
        clipper = new Clipper;
    }
    clipResultUpdate({
        height:"55px",
        opacity:100},"<div id='loading'></div>正在保存分享内容");
    var html = "";
    switch (request.msg) {
        case "sharePage":
            clipper.clipFullPage(true, function (html) {
                title = "分享页面 - " + title;
                weShare(title, html);
            });
            break;
        case "shareSelection":
            clipper.clipSelection(true, request.selectionText, function (html) {
                title = "分享节选 - " + title;
                //alert(html);
                weShare(title, html);
            },function(){
                alert("不好意思出错了")
            });
            break;
        case "shareLink":
            clipper.clipUrl(request.url,undefined,function(html){
                title = "分享链接 - " + title;
                weShare(title, html);
            });
            break;
        case "shareImg":
            clipper.clipImage(request.url,function(html){
                html = "<img src='" + html + "'/>";
                //alert(html);
                title = "分享图片 - " + title;
                weShare(title, html);
            });
            break;
        default:
            break;
    }
});
/**分享
 *
 * @param title 标题
 * @param content 内容
 * @constructor
 */
function weShare(title,content) {
    chrome.extension.sendMessage(
        {
            type: 'gettoken'
        },
        function (e) {
            if (e && e.token && e.token.length > 0) {
                content = content.replace("'", "\'");
                $.ajax({
                    url: "http://www.visten.cn/f/ChromeNote",
                    type: "post",
                    dataType: 'json',
                    data: {title: title, content: content, token: e.token},
                    success: function (json) {
                        showClipResult(json, title, json.Code);
                    },
                    error: function () {
                        showClipResult(json, title, "xxxxxx");
                    }
                });
            }else {
                alert("请先登录");
            }
        });
}
/**显示分享结果页面
 *
 * @param resultJson
 * @param title
 * @param code
 * @constructor
 */
function showClipResult(resultJson, title,code) {
    var share_info = "";
    var url = resultJson.Url;
    if(code==="000200"){
        share_info="已保存到我的笔记本";
    }else if(code==="000000"){
        share_info= resultJson.Msg;
    }else{
        share_info="网络连接失败";
    }
    clipResultUpdate({height:"130px"},share_info,url,title);
}


