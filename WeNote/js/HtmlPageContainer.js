/**
 * Created by Administrator on 2015/1/19.
 */
function clipResultUpdate(attr,result_info,result_link,result_title){
    CreateClipResult(result_info,result_link,result_title,function(){
        var frame = $(this.document).find("#Clip_Result");
        frame.animate(attr,200);
    });
}
function CreateClipResult(result_info,result_link,result_title,animate){
    if($("#Clip_Result").length==0) {
        var a = document.createElement("iframe");
        a.id = "Clip_Result";
        /^frameset$/i.test(document.body.nodeName) ? document.body.parentNode.insertBefore(a, null) : document.body.insertBefore(a, null);
        a.style.width = "380px";
        a.style.height = "0px";
        a.style.position = "fixed";
        a.style.opacity = 0;
        a.style.right = "10px";
        a.style.top = "10px";
        a.style.zIndex = "99999";
        a.style.border = "none";
        a.style.overflow = "hidden";
        a.style.boxShadow= "0 5px 10px rgba(0, 0, 0, 0.6)";
        a.addEventListener("load",function() {
                $(document).find("#Clip_Result")[0].contentWindow.postMessage({
                    name: "ClipResult",
                    share_info: result_info,
                    url: result_link,
                    title: result_title
                }, '*');
            }
        );
        a.src = Browser.extension.getURL("Content/ClipResult.html");
    }else{
        $(document).find("#Clip_Result")[0].contentWindow.postMessage({
            name: "ClipResult",
            share_info: result_info,
            url: result_link,
            title: result_title
        }, '*');
    }
    if(animate){
        animate();
    }
}