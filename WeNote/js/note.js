﻿function submits() {
    var uid = document.getElementById("uid").value;
    var token = document.getElementById("token").value;
    if(uid.length==0 || token.length==0){
        $(this).parent().parent().before("<tr><td class='error' colspan='2'>用户名Token不能为空</td></tr>");
        return;
    }
    $.ajax({
        url: "http://www.visten.cn/f/NoteToken",
        dataType: 'json',
        data: { uid: uid, token: hex_sha1(token) },
        timeout: 5000,
        success: function (jsons) {//客户端jquery预先定义好的callback函数,成功获取跨域服务器上的json数据后,会动态执行这个callback函数 
            if (jsons.id != "-1") {
                if (window.localStorage) {
                    window.localStorage.myName = 'wenote';
                    window.localStorage['token'] = jsons.token;
                    window.localStorage['uid'] = uid;
                    location.reload();
                } else {
                    alert('浏览器不支持localStorage!');
                }
            } else {
                alert("验证失败" + jsons.id);
            }
        },
        complete: function (XMLHttpRequest, textStatus) {
            //$.unblockUI({ fadeOut: 10 }); 
        }
    });
}
function Init() {
    window.localStorage.removeItem("token");
    location.reload();
}
window.onload = function () {
    $("#submit").click(submits);
    $("#submit2").click(Init);
    if (window.localStorage) {
        if (window.localStorage['token']) {
            document.getElementById("form1").style.display = "none";
            document.getElementById("form2").style.display = "block";
            $("#nicname").text("账号 "+window.localStorage['uid'] +" 已经登录")
        } else {
            document.getElementById("form2").style.display = "none";
            document.getElementById("form1").style.display = "block";
        }
    } else {
        alert('浏览器不支持localStorage!');
    }
};