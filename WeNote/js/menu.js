﻿
function genericOnClick(info, tab) {
	alert(info.linkUrl); 
}
function sendToTab(a, b, c) {
    chrome.tabs.sendMessage(a.id, b, c);
};
//分享 所选
function shareSelection(info, tab) {
    sendToTab(tab, {
        msg: "shareSelection",
        name: "startSubmission",
        clipType: "selection",
        contextMenu: !0,
        selectionText: info.selectionText,
        smartFilingNotebookAvailable: !1
    }, function (response) { });
}
//分享 整页
function sharePage(info, tab) {
    chrome.tabs.sendMessage(tab.id,
                        {
                            'msg': 'sharePage'
                        },
                        function (response) {
                            //回传函数    
                        });
}
//分享 地址
function shareLink(info, tab) {
    var url = info.linkUrl ? info.linkUrl : info.pageUrl;
    chrome.tabs.sendMessage(tab.id,
                        {
                            'msg': 'shareLink',
                            'url': url
                        },
                        function (response) {
                            //回传函数    
                        });
}
//分享 地址
function shareImg(info, tab) {
    chrome.tabs.sendMessage(tab.id,
                        {
                            'msg': 'shareImg',
                            'url': info.srcUrl
                        },
                        function (response) {
                            //回传函数    
                        });
}
//添加右键菜单
//var link = chrome.contextMenus.create({ "title": "链接地址", "contexts": ["link"], "onclick": genericOnClick });
/*
* array of enum of "all", "page", "frame", "selection", "link", "editable", "image", "video", "audio", "launcher", "browser_action", or "page_action"
*/
var selection1 = chrome.contextMenus.create({ "title": "收藏整页", "contexts": ["page"], "onclick": sharePage });
var selection2 = chrome.contextMenus.create({ "title": "收藏所选部分", "contexts": ["selection"], "onclick": shareSelection });
var selection3 = chrome.contextMenus.create({ "title": "收藏图片", "contexts": ["image"], "onclick": shareImg });
var selection4 = chrome.contextMenus.create({ "title": "收藏网址", "contexts": ["all"], "onclick": shareLink });

chrome.extension.onMessage.addListener(function (request, sender, sendResponse) {
    if (request && request.type) {
        switch (request.type) {
            case "gettoken":
                //WeShare(request.title, request.content);
                if (sendResponse) {
                    if (window.localStorage['token']) {
                        sendResponse({ token: window.localStorage['token'] });
                    } else {
                        sendResponse({ token: "" });
                    }
                }
                break;
            default:

        }
    }
});

