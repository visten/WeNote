#WeNote

一个简单的Chrome扩展程序
你可以剪切网页上的内容，包括样式。获取拥有style标记的html代码。发布到Note服务器上。
你也可以配置自己的笔记服务器，创建一个属于自己的独一无二的网络笔记本。

---
*仅供学习js使用*

使用方法

```javascript
    clipSelection(bool,selectionText,callback);
```
调用示例

```javascript
    var clipper = new clipper;
    clipper.clipSelection(!0, request.selectionText, function (html) {
                //your code
                //html 是返回回来的html语言，包含了样式等
            });
```

公共Api

| 名称        | 参数   |  类型  |
| --------   | -----:  | :----:  |
| clipFullPage     | 整个网页 |        |
|         |    isIncludeCss    |  bool  |
|         |    callBack    |  function(html)  |
|         |    errorCallBack    |  function(errorMsg)  |
| clipSelection        |   选中的部分   |      |
|         |    isIncludeCss    |  bool  |
|         |    selectionText    |  string  |
|         |    callBack    |  function(html)  |
|         |    errorCallBack    |  function(errorMsg)  |
| clipUrl        |    当前链接或者页面链接    |    |
|         |    url    |  string  |
|         |    title    |  string  |
|         |    callBack    |  function(html)  |
|         |    errorCallBack    |  function(errorMsg)  |
| clipImage        |    当前图片    |    |
|         |    url    |  string  |
|         |    callBack    |  function(html)  |
|         |    errorCallBack    |  function(errorMsg)  |


结果

<http://visten.cn/f/notes-73.html>